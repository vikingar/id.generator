package space.chemodurov.id.generator.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import space.chemodurov.id.generator.entity.Id;
import space.chemodurov.id.generator.entity.Pool;

import javax.annotation.PostConstruct;
import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class GeneratorService {

    @Qualifier("master")
    private final RestTemplate restTemplate;

    private Long id;
    private Long maxValue;

    @PostConstruct
    public void postConstr() {
        updatePool();
    }

    synchronized public Id getId() {
        if (id >= maxValue - 1) {
            updatePool();
        }

        return new Id().setId(id++);
    }

    synchronized private void updatePool() {
        Pool pool = restTemplate.getForObject("/next-pool", Pool.class);
        if (Objects.isNull(pool)) {
            throw new RuntimeException("pool is null");
        }
        id = pool.getMin();
        maxValue = id + (pool.getMax() - pool.getMin());
    }
}
