package space.chemodurov.id.generator.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Id {
    private Long id;
}
