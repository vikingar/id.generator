package space.chemodurov.id.generator.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Pool {
    private Long min;
    private Long max;
}
