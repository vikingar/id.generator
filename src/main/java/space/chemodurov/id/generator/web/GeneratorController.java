package space.chemodurov.id.generator.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import space.chemodurov.id.generator.entity.Id;
import space.chemodurov.id.generator.service.GeneratorService;

@RestController
@RequiredArgsConstructor
@Slf4j
public class GeneratorController {
    private final GeneratorService service;

    @GetMapping()
    public ResponseEntity<Id> getId() {
        return ResponseEntity.ok(service.getId());
    }
}
