package space.chemodurov.id.generator.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

@Configuration
@Slf4j
@RequiredArgsConstructor
public class RestTemplateConfig {
    @Value("${master.url}")
    private String masterUri;

    private final RestTemplateBuilder builder;

    @Bean("master")
    public RestTemplate restTemplate() {
        return builder
                .rootUri(masterUri)
                .build();
    }
}
